﻿using System;
using System.Collections.Generic;

namespace LV3
{
    class Program
    {
        static void Main(string[] args)
        {
            Zadatak1();
            Zadatak2();
            Zadatak3();
            Zadatak4();
        }

        static void Debug(Dataset d)
        {
            foreach (List<string> listString in d.GetData())
            {
                foreach (string s in listString)
                {
                    Console.Write(s);
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }
        static void Debug(float[][] f)
        {
            for (int i = 0; i < f.Length; i++)
            {
                for (int j = 0; j < f[i].Length; j++)
                {
                    Console.Write(f[i][j] + "\t");
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }

        static void Zadatak1()
        {
            Console.WriteLine("Zadatak 1");
            Console.WriteLine("Original");
            Prototype a1 = new Dataset(@"C:\csv.csv");
            Debug((Dataset)a1);
            Console.WriteLine("Deep Copy");
            Prototype a2 = a1.Clone();
            Debug((Dataset)a2);
            Console.WriteLine("***************************************\n");
            /*
             * Duboko kopiranje nije potrebno u ovom primjeru jer ce rezultati biti isti jer Dataset nema slozene tipove podatka koje bi se trebalo duboko kopirati
             * */
        }

        static void Zadatak2()
        {
            Console.WriteLine("Zadatak 2");
            Console.WriteLine("Nasumicna matrica velicine 2x3");
            GeneratorMatrice m = GeneratorMatrice.GetInstance();
            float[][] matrix = m.CreateMatrix(2, 3);
            Debug(matrix);
            Console.WriteLine("***************************************\n");

        }

        static void Zadatak3()
        {
            Console.WriteLine("Zadatak 3");
            Console.WriteLine("U C:log.txt je napisana jedna recenica sa dvije funkcije.");
            Logger logger = Logger.GetInstance();
            logger.SetFilepath(@"C:\logger.txt");
            Zadatak3_1();
            Zadatak3_2();
            Console.WriteLine("\n***************************************\n");
            //Uporaba loggera na drugim mjestima će pisati u istu datoteku jer je logger singleton
        }
        static void Zadatak3_1()
        {
            Logger logger = Logger.GetInstance();
            logger.Log("Ovo je pocetak ");
        }
        static void Zadatak3_2()
        {
            Logger logger = Logger.GetInstance();
            logger.Log("nekakve recenice!");
        }

        static void Zadatak4()
        {
            Console.WriteLine("Zadatak 4");
            Console.WriteLine("Testiranje klase sa podatcima: Anonymous, Notification, NotificationText, CurrTime, ERROR, Red");
            ConsoleNotification notif = new ConsoleNotification("Anonymous", "Notification", "NotificationText", DateTime.Now, Category.ALERT, ConsoleColor.Red);
            NotificationManager manager = new NotificationManager();
            manager.Display(notif);
            Console.WriteLine("\n***************************************\n");
        }
    }
}
