﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV3
{
    class Logger
    {
        public static Logger instance;
        string filePath;

        private Logger()
        {
            filePath = @"C:\datoteka.txt";
        }

        public static Logger GetInstance()
        {
            if (instance == null)
            {
                instance = new Logger();
            }
            return instance;
        }

        public void Log(string message)
        {
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(filePath, true))
            {
                file.Write(message);
            }
        }

        public void SetFilepath(string filepathIn)
        {
            filePath = filepathIn;
        }
    }
}