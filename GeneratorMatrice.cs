﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV3
{
    class GeneratorMatrice
    {
        public static GeneratorMatrice instance;
        private Random random;

        private GeneratorMatrice()
        {
            random = new Random();
        }

        public static GeneratorMatrice GetInstance()
        {
            if (instance == null)
            {
                instance = new GeneratorMatrice();
            }
            return instance;
        }

        //Zadatak 2
        public float[][] CreateMatrix(int width, int height)
        {
            float[][] matrix = new float[height][];
            for (int i = 0; i < matrix.Length; i++)
            {
                matrix[i] = new float[width];
            }

            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    matrix[i][j] = (float)random.NextDouble();
                }
            }

            return matrix;
        }
    }
}